# chaiscript for integration testing - a proof-of-concept #

This repository contains the code for an article about using ChaiScript
for integration testing [hosted on my blog](https://alfateam123.nwa.xyz/blog/posts/embeddable-scripting-languages-are-for-integration-testing-too.html)

### Dependencies ###

* zeromq

* chaiscript

* a C++14 compiler (gcc/clang)

Both are supposed to be built by source. Please modify the Makefile in the repository
if you installed zeromq via your OS package manager.

### How to build it ###

clone the project, modify Makefile to set the paths for dependency, run `make` to build both the server
and the client.

### How to run it ###

1. Open two terminals and `cd` into the cloned project.

2. In the first, run `make run_server`

3. In the second, run `make run_client`

You should see some printing in both terminals. Check the article for the explanation of the prints you see on the screen!