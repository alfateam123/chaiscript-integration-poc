#include <chaiscript/chaiscript.hpp>
#include <chaiscript/utility/utility.hpp>
#include <iostream>
#include <clientclass.h>

int main(int argc, char* argv[]) {
  // handle parameters. if no parameter is given, use "test.chai"
  // as default script name.
  std::string script_name = "test.chai";
  if(argc == 2){
    script_name = std::string(argv[1]);
  }
  else if(argc > 2){
    std::cout << "usage: " << argv[0] << " <script.chai>" << std::endl;
    return 1;
  }

  // create the ChaiScript interpreter
  chaiscript::ChaiScript chai;

  // extend ChaiScript with our ClientClass class,
  // so it can recognize our class' usage in the module,
  // call the right function at the right time, etc.
  chaiscript::ModulePtr m = chaiscript::ModulePtr(new chaiscript::Module());
  chaiscript::utility::add_class<ClientClass>(*m,
     "ClientClass",
     { chaiscript::constructor<ClientClass(const std::string)>()},
     { {chaiscript::fun(&ClientClass::logDebug), "logDebug"},
       {chaiscript::fun(&ClientClass::logValue), "logValue"}
     });
  chai.add(m);

  // the (augmented) interpreter picks the script and executes it.
  // it never asked for this, by the way.
  chai.eval_file(script_name);

  return 0;
}
