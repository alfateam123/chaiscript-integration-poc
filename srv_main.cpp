#include <zmq.h>
#include <iostream>
#include <cstring>

int main() {
  void *context = zmq_ctx_new();
  void *socket_ = zmq_socket(context, ZMQ_ROUTER);

  // set a timeout
  unsigned int recv_timeout_ms = 10000;
  zmq_setsockopt(socket_, ZMQ_RCVTIMEO, &recv_timeout_ms,
                 sizeof(recv_timeout_ms));

  if (zmq_bind(socket_, "tcp://127.0.0.1:6987") == -1) {
    std::cout << "err: could not bind, " << zmq_strerror(zmq_errno())
              << std::endl;
    return 1;
  }

  // needed to skip identity messages
  bool is_recv_message_identity = false;
  bool timeoutted = false;

  while (!timeoutted) {
    char buffer[20];
    bzero(buffer, 20 * sizeof(char));
    int recv_res = zmq_recv(socket_, buffer, 19, 0);

    if (recv_res == -1) {
      if (zmq_errno() == 11) { // EAGAIN
        timeoutted = true;
        continue;
      } else {
        std::cout << "err: could not receive [" << zmq_errno() << "] "
                  << zmq_strerror(zmq_errno()) << std::endl;
        return 2;
      }
    }

    // skip identity messages: they're not the payload we are looking for,
    //      and we don't need to do identity-based logic in this PoC.
    is_recv_message_identity = is_recv_message_identity ? false : true;
    if (is_recv_message_identity)
      continue;

    if (buffer[0] == '\0') {
      std::cout << "received a string!" << std::endl;
      std::cout << "the received string is: >>>" << std::string(buffer + 1)
                << "<<<" << std::endl;
    } else {
      std::cout << "received a number!" << std::endl;
      std::cout << "the received number is: " << (int)buffer[1] << std::endl;
    }
  }

  int sock_res = zmq_close(socket_);
  if (sock_res == -1)
    return sock_res;
  int ctx_res = zmq_ctx_shutdown(context);
  return ctx_res;
}
