# Note: we assume that every dependency (zeromq, chaiscript)
#       is installed inside $HOME/built/{zeromq,chai}.
#       this path can be set using `./configure --install-prefix=$HOME/zeromq`
#       and `cmake . -DCMAKE_INSTALL_PREFIX=$HOME/chaiscript`
#       when building dependencies from sources (as I always do when developing).
# Requirements: a C++14-capable compiler. tested with g++ 5.3+ and clang++ 3.8+
#               a *nix operating system. The code should be cross-platform, though.
# Run different scripts: just modify the recipe for `run_client`.

all: build

build: build_server build_client

build_server:
	g++ -I${HOME}/built/zeromq/include \
	    -L${HOME}/built/zeromq/lib -lzmq -o server srv_main.cpp

build_client:
	g++ -Wall -std=c++14 -I${HOME}/built/chai/include \
	    -L${HOME}/built/chai/lib/chaiscript -lchaiscript_stdlib-6.0.0 -ldl \
	    -I${HOME}/built/zeromq/include \
	    -L${HOME}/built/zeromq/lib -lzmq \
	    -lpthread -I. clientclass.cpp clientclass.h cln_main.cpp -o client

run_server:
	./server

run_client:
	LD_LIBRARY_PATH="${HOME}/built/chai/lib/chaiscript/" ./client


clean:
	rm server client
