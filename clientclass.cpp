#include <clientclass.h>
#include <cstring>
#include <assert.h>

ClientClass::ClientClass(const std::string address){
  this->address = address;
  
  context = zmq_ctx_new();
  socket = zmq_socket(context, ZMQ_DEALER);

  zmq_connect(socket, address.c_str());  
}

ClientClass::~ClientClass(){
  zmq_disconnect(socket, address.c_str());
  zmq_close(socket);
  zmq_ctx_shutdown(context);
  zmq_ctx_term(context);
}

void ClientClass::logDebug(std::string phrase_to_send) {
  std::cout << "sending the string: " << phrase_to_send << std::endl;
  char *buffer = new char[2 + phrase_to_send.size()];
  buffer[0] = 0;
  strncpy(buffer+1, phrase_to_send.c_str(), phrase_to_send.size()+1);

  int res = zmq_send(socket, buffer, 2 + phrase_to_send.size(), 0);
  assert(res != -1);

  delete[] buffer;
}

void ClientClass::logValue(uint8_t value_to_send) {
  std::cout << "sending value: " << (int)value_to_send << std::endl;
  char *buffer = new char[2];
  buffer[0] = 1;
  buffer[1] = value_to_send;

  int res = zmq_send(socket, buffer, 2, 0);
  assert(res == 2);

  delete[] buffer;
}
