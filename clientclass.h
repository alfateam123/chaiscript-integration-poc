#include <zmq.h>
#include <iostream>

class ClientClass {
private:
  void *context;
  void *socket;
  std::string address;

public:
  ClientClass(const std::string address);
  ~ClientClass();
  //ClientClass(ClientClass&) = delete;

  void logDebug(std::string phrase_to_send);
  void logValue(uint8_t value_to_send);
};
